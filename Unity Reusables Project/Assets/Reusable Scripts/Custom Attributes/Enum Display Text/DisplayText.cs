﻿//-----------------------------------------------------------------------
// <copyright file="DisplayText.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.CustomAttributes
{
    using System;

    /// <summary>
    /// Gives display text to whatever this attribute is attached to. 
    /// </summary>
    /// <remarks>
    /// I got a majority of this code off of forums. I don't remember where
    /// it was retrieved from, so if you recognize it, let me know so I can
    /// give credit where credit is due.
    /// </remarks>
    public class DisplayText : Attribute
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the DisplayText class. 
        /// </summary>
        /// <param name="text">
        /// The text to display. 
        /// </param>
        public DisplayText(string text)
        {
            Text = text;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets or sets the text that should be displayed. 
        /// </summary>
        public string Text { get; set; }

        #endregion Public Properties
    }
}