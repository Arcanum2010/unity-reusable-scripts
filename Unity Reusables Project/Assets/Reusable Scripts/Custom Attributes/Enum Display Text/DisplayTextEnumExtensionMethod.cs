﻿//-----------------------------------------------------------------------
// <copyright file="DisplayTextEnumExtensionMethod.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.CustomAttributes
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Extension methods to support the display text attribute. 
    /// </summary>
    /// <remarks>
    /// I got a majority of this code off of forums. I don't remember where
    /// it was retrieved from, so if you recognize it, let me know so I can
    /// give credit where credit is due.
    /// </remarks>
    public static class DisplayTextEnumExtensionMethod
    {
        #region Public Methods

        /// <summary>
        /// Displays the custom attribute information from the display text attribute.
        /// </summary>
        /// <param name="en">
        /// The enumerated value to get text for. 
        /// </param>
        /// <returns>
        /// The string represented in DisplayText if one is there.
        /// Otherwise will return the generic ToString call of the enumerated.
        /// </returns>
        public static string ToDisplay(this Enum en)
        {
            // Get the type and member information 
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            // If the memInfo has no information then return the basic to
            // string of enumeration
            if (memInfo.Length <= 0) return en.ToString();

            // Get the attributes on the enum members 
            object[] attrs = memInfo[0].GetCustomAttributes(typeof(DisplayText), false);

            // If there is an attribute then return the display text
            // Otherwise, return the basic to string of enumeration
            return attrs.Length > 0 ? ((DisplayText)attrs[0]).Text : en.ToString();
        }

        #endregion Public Methods
    }
}