﻿namespace ReusableScripts.MonoBehaviours
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public abstract class AdvancedMonoBehaviourWithComponents : AdvancedMonoBehaviour
    {
        /// <summary>
        /// A static list of all the component references in relation
        /// to the game objects that are associated with.
        /// </summary>
        private static Dictionary<GameObject, List<Component>> _allComponents;

        /// <summary>
        /// Gets the component reference of a specific game object.
        /// </summary>
        /// <typeparam name="T">The type of component that you want a reference to.</typeparam>
        /// <param name="gObject">The game object that the component reference should be attached to.</param>
        /// <returns>The component reference that was requested.</returns>
        public static T AdvancedGetComponent<T>(GameObject gObject) where T : Component
        {
            return (T)_allComponents[gObject].FirstOrDefault(x => x.GetType() == typeof(T));
        }

        /// <summary>
        /// Gets the component refrences of multiple game objects.
        /// </summary>
        /// <typeparam name="T">The component that you want reference to from all of the game objects.</typeparam>
        /// <param name="gObjects">The game objects that you want reference to.</param>
        /// <returns>An array of components of type T that were attached to the supplied game objects.</returns>
        public static T[] AdvancedGetComponents<T>(params GameObject[] gObjects) where T : Component
        {
#if UNITY_IOS
            List<T> componentReferences = new List<T>();
            foreach (GameObject curGo in gObjects)
            {
                foreach (Component curComponent in _allComponents[curGo])
                {
                    if (curComponent.GetType() == typeof(T))
                        componentReferences.Add((T)curComponent);
                }
            }
            return componentReferences.ToArray();
#else
            return (T[])_allComponents.Where(x => gObjects.Contains(x.Key)).Select(x => x.Value.Where(y => y.GetType() == typeof(T))).ToArray();
#endif
        }

        /// <summary>
        /// Gets the component refrences of multiple game objects.
        /// </summary>
        /// <typeparam name="T">The component that you want reference to from all of the game objects.</typeparam>
        /// <param name="gObjects">The game objects that you want reference to.</param>
        /// <returns>An array of components of type T that were attached to the supplied game objects.</returns>
        public static T[] AdvancedGetComponents<T>(List<GameObject> gObjects) where T : Component
        {
#if UNITY_IOS
            GameObject[] gameObjectArray = new GameObject[gObjects.Count];
            for (int i = 0; i < gObjects.Count;i++)
            {
                gameObjectArray[i] = gObjects[i];
            }
            return AdvancedGetComponents<T>(gameObjectArray);
#else
            return AdvancedGetComponents<T>(gObjects.ToArray());
#endif
        }

        /// <summary>
        /// Gets an array of all the components of a type in the scene.
        /// </summary>
        /// <typeparam name="T">Component type that will be searched for.</typeparam>
        /// <returns>Array of all components of type supplied.</returns>
        public static T[] GetAllComponentsOfType<T>() where T : Component
        {
#if UNITY_IOS
            List<T> returnList = new List<T>();
            foreach (List<Component> curListComponent in _allComponents.Values)
            {
                foreach (Component curComponent in curListComponent)
                {
                    if (curComponent.GetType() == typeof(T))
                        returnList.Add(curComponent as T);
                }
            }
            return returnList.ToArray();
#else
            return (T[])_allComponents.Values.Select(x => x.Where(y => y.GetType() == typeof(T))).ToArray();
#endif
        }

        /// <summary>
        /// Gets a component from a game object.
        /// </summary>
        /// <typeparam name="T">
        /// The type of component being referenced.
        /// </typeparam>
        /// <returns>
        /// A component of type T that is attached to this game object.
        /// </returns>
        public T AdvancedGetComponent<T>() where T : Component
        {
#if UNITY_IOS
            foreach(Component curComponent in _allComponents[gameObject])
            {
                if(curComponent.GetType() == typeof(T))
                {
                    return curComponent as T;
                }
            }
            return null;
#else
            return (T)_allComponents[gameObject].FirstOrDefault(x => x.GetType() == typeof(T));
#endif
        }

        /// <summary>
        /// Adds a component to a static list for reference.
        /// </summary>
        /// <typeparam name="T">
        /// The type of component that is to be added.
        /// </typeparam>
        /// <param name="addToGameobject">
        /// Whether or not to add the component to the game object.
        /// </param>
        /// <returns>
        /// The component reference of type that was added to the game object.
        /// </returns>
        /// <remarks>
        /// If this component already exists on the game object, then you should not add it to the game object.
        /// If this component does not already exist on the game object, then you should add it to the game object.
        /// </remarks>
        protected T AdvancedAddComponent<T>(bool addToGameobject = true) where T : Component
        {
            // Reference for the component
            T componentReference =
                addToGameobject ?
                (T)gameObject.AddComponent(typeof(T)) :
                (T)gameObject.GetComponent(typeof(T));

            // Add the component reference to the list
            _allComponents[gameObject].Add(componentReference);

            // Return the component
            return componentReference;
        }

        /// <summary>
        /// Adds a component reference to a static list for reference.
        /// </summary>
        /// <typeparam name="T">
        /// The type of component to add a reference to.
        /// </typeparam>
        /// <param name="gameObjectToReference">
        /// The game object that the component is attached to.
        /// </param>
        /// <param name="addToGameObject">
        /// Whether or not to add the component to the game object.
        /// </param>
        /// <returns>
        /// The component reference of type that was added to the game object.
        /// </returns>
        protected T AdvancedAddComponent<T>(GameObject gameObjectToReference, bool addToGameObject = true) where T : Component
        {
            // Reference for the component
            T componentReference =
                addToGameObject ?
                (T)gameObjectToReference.AddComponent(typeof(T)) :
                (T)gameObjectToReference.GetComponent(typeof(T));

            // Add the component reference to the list
            _allComponents[gameObjectToReference].Add(componentReference);

            // Return the component
            return componentReference;
        }

        /// <summary>
        /// Method that will be called during OnDestroy.
        /// </summary>
        /// <remarks>
        /// Use this method instead of OnDestroy so that base calls from AdvancedMonoBehaviour can
        /// be called as well. This will still be called during the OnDestroy call.
        /// </remarks>
        protected abstract void AdvancedOnDestroy();

        /// <summary>
        /// Called when the MonoBehaviour will be destroyed.
        /// </summary>
        /// <remarks>
        /// Do not use this method! Instead use the abstract method of AdvancedOnDestroy().
        /// </remarks>
        protected void OnDestroy()
        {
            // Remove this gameobject in the component reference list
            _allComponents.Remove(gameObject);

            // Call the abstract advanced OnDestroy method
            AdvancedOnDestroy();
        }

        protected override void AdvancedAwake()
        {
            // Initial set of the _allComponents static list
            if (_allComponents == null) _allComponents = new Dictionary<GameObject, List<Component>>();

            // Add this gameobject in the component reference list
            if (!_allComponents.ContainsKey(gameObject))
                _allComponents.Add(gameObject, new List<Component>(gameObject.GetComponents(typeof(Component))));
        }
    }
}