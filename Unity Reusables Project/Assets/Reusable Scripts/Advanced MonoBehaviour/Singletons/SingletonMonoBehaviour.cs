﻿//-----------------------------------------------------------------------
// <copyright file="SingletonMonoBehaviour.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.MonoBehaviours
{
    using UnityEngine;

    /// <summary>
    /// Provides singleton functionality in the inherited function. 
    /// </summary>
    /// <typeparam name="T">
    /// The class that is inheriting from the singleton monobehaviour class. 
    /// </typeparam>
    /// <remarks>
    /// Also inherits from the advanced monobehaviour and contains all subsequent functionality. 
    /// </remarks>
    public abstract class SingletonMonoBehaviour<T> : AdvancedMonoBehaviour where T : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// The private reference holder to the singleton instance of T class. 
        /// </summary>
        private static T _instance;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets a reference to the singleton instance of T class. 
        /// </summary>
        public static T Instance
        {
            get { return _instance; }
        }

        /// <summary>
        /// Determines if the instance is currently null. 
        /// </summary>
        public static bool InstanceIsNull
        {
            get
            {
                return _instance == null;
            }
        }

        #endregion Public Properties

        #region Protected Methods

        /// <summary>
        /// Method that will be called during Awake. 
        /// </summary>
        /// <remarks>
        /// Use this method instead of awake so that base calls from AdvancedMonoBehaviour can be
        /// called as well. This will still be called during the awake call. Call the base function
        /// of this class in order for the singleton to work properly.
        /// </remarks>
        protected override void AdvancedAwake()
        {
            // Check to see if this is the second of this singleton 
            if (_instance != null && _instance != this)
                Destroy(gameObject);

            // Assign the singleton instance 
            _instance = this as T;
        }

        /// <summary>
        /// Method that will be called during OnDestroy. 
        /// </summary>
        /// <remarks>
        /// Use this method instead of OnDestroy so that base calls from SingletonMonoBehaviour can
        /// be called as well. This will still be called during the OnDestroy call.
        /// </remarks>
        protected abstract void AdvancedOnDestroy();

        /// <summary>
        /// Called when the MonoBehaviour will be destroyed. 
        /// </summary>
        protected void OnDestroy()
        {
            // If the instance is not set to null. If you go back to the scene again later, the
            // _instance will throw a null reference error. Also check to see if this instance is
            // this one. If this is just a second singleton, we don't want to break the reference to
            // the real singleton.
            if (_instance == this)
            {
                _instance = null;
            }

            // Call the advanced on destroy method. 
            AdvancedOnDestroy();
        }

        #endregion Protected Methods
    }
}