﻿namespace ReusableScripts.Grids.Squares
{
    using MonoBehaviours;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class SquareGrid : AdvancedMonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// Number of columns that the square grid should contain.
        /// </summary>
        [SerializeField]
        private int _columns;

        /// <summary>
        /// The padding to add around all the pieces in the grid.
        /// </summary>
        [SerializeField]
        private float _padding;

        /// <summary>
        /// The scale of the square piece.
        /// </summary>
        /// <remarks>
        /// Vector3.one will not scale the square pieces.
        /// </remarks>
        [SerializeField]
        private Vector3 _pieceScale;

        /// <summary>
        /// Number of rows that the square grid should contain.
        /// </summary>
        [SerializeField]
        private int _rows;

        /// <summary>
        /// The square prefab to use as the square pieces.
        /// </summary>
        [SerializeField]
        private GameObject _squarePrefab;

        /// <summary>
        /// List of squares that belong to this grid.
        /// </summary>
        private List<Square> _squares;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Create the grid.
        /// </summary>
        public void CreateGrid()
        {
            if (_rows == 0 || _columns == 0 || _squarePrefab == null)
            {
                return;
            }

            if (_squares == null)
            {
                _squares = new List<Square>();
            }

            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    MakeSquare(i, j);
                }
            }

            SizeUpdate();
        }

        /// <summary>
        /// Update the grid with the current information.
        /// </summary>
        public void UpdateGrid()
        {
            // Make sure all information is present in order to update
            if (_rows == 0 || _columns == 0 || _squarePrefab == null || _squares == null)
            {
                return;
            }

            foreach (Square curSquare in _squares)
            {
                UpdateSquare(curSquare);
            }
        }

        #endregion Public Methods

        #region Protected Methods

        protected override void AdvancedAwake()
        {
            if (_squares == null)
            {
                _squares = new List<Square>();
            }
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Clear the squares that are no longer defined.
        /// </summary>
        private void ClearUndefinedSquares()
        {
            for (int i = _squares.Where(x => x.Row >= _rows || x.Column >= _columns).Count(); i > 0; i--)
            {
                Square curSquare = _squares.First(x => x.Row >= _rows || x.Column >= _columns);
                DestroyImmediate(curSquare.gameObject);
                _squares.Remove(curSquare);
            }
        }

        /// <summary>
        /// Get the local position of the square.
        /// </summary>
        /// <param name="row">The row that the square is in.</param>
        /// <param name="col">The column that the square is in.</param>
        /// <returns>A vector of the squares location.</returns>
        private Vector3 GetLocalPosition(int row, int col)
        {
            return new Vector3(
                col * (_pieceScale.x + _padding),
                0,
                row * (_pieceScale.z + _padding)
                );
        }

        /// <summary>
        /// Make a square object to be placed inside the grid.
        /// </summary>
        /// <param name="row">The row that the square should be in.</param>
        /// <param name="column">The column that the square should be in.</param>
        private void MakeSquare(int row, int column)
        {
            GameObject squareObj = (GameObject)Instantiate(_squarePrefab, GetLocalPosition(row, column), Quaternion.identity);
            Square curSquare = squareObj.GetComponent<Square>() == null ? squareObj.AddComponent<Square>() : squareObj.GetComponent<Square>();
            curSquare.Setup(row, column);
            squareObj.transform.SetParent(transform);
            _squares.Add(curSquare);
        }

        /// <summary>
        /// Update the size of a square.
        /// </summary>
        /// <param name="square">The square whos size needs to be updated.</param>
        private void SizeUpdate(GameObject square)
        {
            square.transform.localScale = Vector3.Scale(_squarePrefab.transform.localScale, _pieceScale);
        }

        /// <summary>
        /// Update the size of all the squares.
        /// </summary>
        private void SizeUpdate()
        {
            foreach (Square curSquare in _squares)
            {
                SizeUpdate(curSquare.gameObject);
            }
        }

        /// <summary>
        /// Update a square object.
        /// </summary>
        /// <param name="square">Square object to update.</param>
        private void UpdateSquare(Square square)
        {
            square.transform.localPosition = GetLocalPosition(square.Row, square.Column);
        }

        #endregion Private Methods
    }
}