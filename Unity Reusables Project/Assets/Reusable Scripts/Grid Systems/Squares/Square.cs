﻿using UnityEngine;

public class Square : MonoBehaviour
{
    #region Private Fields

    [SerializeField]
    private int _col;

    [SerializeField]
    private int _row;

    #endregion Private Fields

    #region Public Properties

    public int Column
    {
        get
        {
            return _col;
        }
    }

    public int Row
    {
        get
        {
            return _row;
        }
    }

    #endregion Public Properties

    #region Public Methods

    public void Setup(int row, int column)
    {
        _row = row;
        _col = column;
    }

    #endregion Public Methods
}