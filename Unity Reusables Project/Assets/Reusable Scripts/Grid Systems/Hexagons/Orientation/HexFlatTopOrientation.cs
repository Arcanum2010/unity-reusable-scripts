﻿namespace ReusableScripts.Grids.Hexagons.Orientation
{
    using UnityEngine;

    public class HexFlatTopOrientation : IHexOrientation
    {
        /// <summary>
        /// Gets the starting rotation of the orientation.
        /// </summary>
        public Quaternion StartingRotation
        {
            get
            {
                return Quaternion.identity;
            }
        }

        /// <summary>
        /// Calculates the x position of a piece at a particular coordinate.
        /// </summary>
        /// <param name="x">The x axial coordinate of the hexagon.</param>
        /// <param name="z">The z axial coordinate of the hexagon.</param>
        /// <param name="size">The width of the hexagon.</param>
        /// <param name="padding">The distance between hexagons.</param>
        /// <param name="scale">The scale of the hexagon from the original position.</param>
        /// <returns>The x position relative to the parent grid object.</returns>
        public float GetXPosition(Hexagon hex, float size, float padding, float scale)
        {
            float xSize = scale + size + padding;
            return xSize * (3.0f / 2.0f) * hex.X;
        }

        /// <summary>
        /// Calculates the z position of a piece at a particular coordinate.
        /// </summary>
        /// <param name="x">The x axial coordinate of the hexagon.</param>
        /// <param name="z">The z axial coordinate of the hexagon.</param>
        /// <param name="size">The height of the hexagon.</param>
        /// <param name="padding">The distance between hexagons.</param>
        /// <param name="scale">The scale of the hexagon from the original position.</param>
        /// <returns>The z position relative to the parent grid object.</returns>
        public float GetZPosition(Hexagon hex, float size, float padding, float scale)
        {
            float zSize = scale + size + padding;
            return zSize * Mathf.Sqrt(3) * (hex.Z + (hex.X / 2.0f));
        }
    }
}