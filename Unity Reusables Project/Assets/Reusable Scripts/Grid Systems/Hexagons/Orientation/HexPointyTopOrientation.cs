﻿namespace ReusableScripts.Grids.Hexagons.Orientation
{
    using UnityEngine;

    public class HexPointyTopOrientation : IHexOrientation
    {
        #region Public Properties

        /// <summary>
        /// Gets the starting rotation of the orientation.
        /// </summary>
        public Quaternion StartingRotation
        {
            get
            {
                return Quaternion.Euler(0, 0, 30);
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Calculates the x position of a piece at a particular coordinate.
        /// </summary>
        /// <param name="x">The x axial coordinate of the hexagon.</param>
        /// <param name="z">The z axial coordinate of the hexagon.</param>
        /// <param name="size">The width of the hexagon.</param>
        /// <param name="padding">The distance between hexagons.</param>
        /// <param name="scale">The scale of the hexagon from the original position.</param>
        /// <returns>The x position relative to the parent grid object.</returns>
        public float GetXPosition(Hexagon hex, float size, float padding, float scale)
        {
            float sizeX = scale + size + padding;
            return sizeX * Mathf.Sqrt(3) * (hex.X + (hex.Z / 2.0f));
        }

        /// <summary>
        /// Calculates the z position of a piece at a particular coordinate.
        /// </summary>
        /// <param name="x">The x axial coordinate of the hexagon.</param>
        /// <param name="z">The z axial coordinate of the hexagon.</param>
        /// <param name="size">The height of the hexagon.</param>
        /// <param name="padding">The distance between hexagons.</param>
        /// <param name="scale">The scale of the hexagon from the original position.</param>
        /// <returns>The z position relative to the parent grid object.</returns>
        public float GetZPosition(Hexagon hex, float size, float padding, float scale)
        {
            float sizeZ = scale + size + padding;
            return sizeZ * (3.0f / 2.0f) * hex.Z;
        }

        #endregion Public Methods
    }
}