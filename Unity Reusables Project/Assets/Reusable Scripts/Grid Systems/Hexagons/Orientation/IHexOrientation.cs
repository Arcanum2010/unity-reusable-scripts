﻿namespace ReusableScripts.Grids.Hexagons.Orientation
{
    using UnityEngine;

    public interface IHexOrientation
    {
        #region Public Properties

        /// <summary>
        /// Gets the starting rotation of the orientation.
        /// </summary>
        Quaternion StartingRotation { get; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Calculates the x position of a piece at a particular coordinate.
        /// </summary>
        /// <param name="x">The x axial coordinate of the hexagon.</param>
        /// <param name="z">The z axial coordinate of the hexagon.</param>
        /// <param name="size">The width of the hexagon.</param>
        /// <param name="padding">The distance between hexagons.</param>
        /// <param name="scale">The scale of the hexagon from the original position.</param>
        /// <returns>The x position relative to the parent grid object.</returns>
        float GetXPosition(Hexagon hex, float size, float padding, float scale);

        /// <summary>
        /// Calculates the z position of a piece at a particular coordinate.
        /// </summary>
        /// <param name="x">The x axial coordinate of the hexagon.</param>
        /// <param name="z">The z axial coordinate of the hexagon.</param>
        /// <param name="size">The height of the hexagon.</param>
        /// <param name="padding">The distance between hexagons.</param>
        /// <param name="scale">The scale of the hexagon from the original position.</param>
        /// <returns>The z position relative to the parent grid object.</returns>
        float GetZPosition(Hexagon hex, float size, float padding, float scale);

        #endregion Public Methods
    }
}