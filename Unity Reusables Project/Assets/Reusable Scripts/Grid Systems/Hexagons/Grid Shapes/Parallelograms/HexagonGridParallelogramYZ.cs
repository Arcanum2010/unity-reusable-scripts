﻿namespace ReusableScripts.Grids.Hexagons
{
    public class HexagonGridParallelogramYZ : HexagonGrid
    {
        #region Public Methods

        /// <summary>
        /// Creates the grid with all the pieces.
        /// </summary>
        public override void CreateGrid()
        {
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    MakeHexagon(-i - j, i, j);
                }
            }
        }

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Clear any hexagon that is no longer defined in the grid.
        /// </summary>
        protected override void ClearUndefinedHexagons()
        {
            for (int i = 0; i <= _hexagons.Count; i++)
            {
                if (_hexagons[i].Y >= _rows || _hexagons[i].Z >= _columns)
                {
                    DestroyImmediate(_hexagons[i].gameObject);
                    _hexagons.Remove(_hexagons[i]);
                }
            }
        }

        #endregion Protected Methods
    }
}