﻿namespace ReusableScripts.Grids.Hexagons
{
    using System;

    public class HexagonGridRectangleXY : HexagonGrid
    {
        #region Public Methods

        /// <summary>
        /// Creates the grid with all the pieces.
        /// </summary>
        public override void CreateGrid()
        {
            for (int i = 0; i < _rows; i++)
            {
                int rowOffset = (int)Math.Floor((i + _pieceSize.x) / 2);
                for (int j = -rowOffset; j < _columns - rowOffset; j++)
                {
                    MakeHexagon(i, j, -i - j);
                }
            }
        }

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Clear any hexagon that is no longer defined in the grid.
        /// </summary>
        protected override void ClearUndefinedHexagons()
        {
            for (int i = 0; i <= _hexagons.Count; i++)
            {
                if (_hexagons[i].X >= _rows || _hexagons[i].Y >= _columns)
                {
                    DestroyImmediate(_hexagons[i].gameObject);
                    _hexagons.Remove(_hexagons[i]);
                }
            }
        }

        #endregion Protected Methods
    }
}