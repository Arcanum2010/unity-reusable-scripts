﻿namespace ReusableScripts.Grids.Hexagons
{
    using System;

    public class HexagonGridHexagons : HexagonGrid
    {
        #region Public Methods

        /// <summary>
        /// Creates the grid with all the pieces.
        /// </summary>
        public override void CreateGrid()
        {
            // Check to make sure that the columns and rows are in correct proportion with each other
            if (_rows != _columns)
            {
                throw new ArgumentException(
                    string.Format(
                        "The rows must be equal to the columns for a hexagon grid to be generated.\nRows: {0}\nColumns: {1}",
                        _rows,
                        _columns));
            }

            for (int i = -_rows; i <= _rows; i++)
            {
                int radiusOne = Math.Max(-_rows, -i - _rows);
                int radiusTwo = Math.Min(_rows, -i + _rows);
                for (int j = radiusOne; j <= radiusTwo; j++)
                {
                    MakeHexagon(i, j, -i - j);
                }
            }
        }

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Clear any hexagon that is no longer defined in the grid.
        /// </summary>
        protected override void ClearUndefinedHexagons()
        {
            for (int i = 0; i <= _hexagons.Count; i++)
            {
                if (_hexagons[i].X > _rows || _hexagons[i].Y > _columns)
                {
                    DestroyImmediate(_hexagons[i].gameObject);
                    _hexagons.Remove(_hexagons[i]);
                }
            }
        }

        #endregion Protected Methods
    }
}