﻿namespace ReusableScripts.Grids.Hexagons
{
    using System;

    public class HexagonGridTriangleUpLeft : HexagonGrid
    {
        #region Public Methods

        /// <summary>
        /// Creates the grid with all the pieces.
        /// </summary>
        public override void CreateGrid()
        {
            // Check to make sure that the columns and rows are in correct proportion with each other
            if (_rows != _columns)
            {
                throw new ArgumentException(
                    string.Format(
                        "The rows must be equal to the columns for a triangle grid to be generated.\nRows: {0}\nColumns: {1}",
                        _rows,
                        _columns));
            }

            for (int i = 0; i <= _rows; i++)
            {
                for (int j = _rows - i; j <= _rows; j++)
                {
                    MakeHexagon(i, j, -i - j);
                }
            }
        }

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Clear any hexagon that is no longer defined in the grid.
        /// </summary>
        protected override void ClearUndefinedHexagons()
        {
            for (int i = 0; i <= _hexagons.Count; i++)
            {
                if (_hexagons[i].X > _rows || _hexagons[i].Y > _columns)
                {
                    DestroyImmediate(_hexagons[i].gameObject);
                    _hexagons.Remove(_hexagons[i]);
                }
            }
        }

        #endregion Protected Methods
    }
}