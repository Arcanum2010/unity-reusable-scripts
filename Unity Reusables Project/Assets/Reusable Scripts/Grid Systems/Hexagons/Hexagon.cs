﻿namespace ReusableScripts.Grids.Hexagons
{
    using System;
    using UnityEngine;

    public class Hexagon : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// The x axial coordinate for this hexagon.
        /// </summary>
        [SerializeField]
        private int _x;

        /// <summary>
        /// The z axial coordinate for this hexagon.
        /// </summary>
        [SerializeField]
        private int _z;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets the x cubal coordinate for this hexagon.
        /// </summary>
        public int X
        {
            get
            {
                return _x;
            }
        }

        /// <summary>
        /// Gets the y cubal coordinate for this hexagon.
        /// </summary>
        public int Y
        {
            get
            {
                return -_x - _z;
            }
        }

        /// <summary>
        /// Gets the z cubal coordinate for this hexagon.
        /// </summary>
        public int Z
        {
            get
            {
                return _z;
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Check to see if a hexagon's values are equal to each other.
        /// </summary>
        /// <param name="hexOne">The first hexagon to be compared.</param>
        /// <param name="hexTwo">The second hexagon to be compared.</param>
        /// <returns>True if both hexagon's share the same coordinate values.</returns>
        public static bool HexagonValuesEqual(Hexagon hexOne, Hexagon hexTwo)
        {
            if (hexOne.X == hexTwo.X && hexOne.Y == hexTwo.Y)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check to see if a hexagon's values are equal to this ones.
        /// </summary>
        /// <param name="hex">The hexagon to test against.</param>
        /// <returns>True if the hexagon's share the same coordinate values.</returns>
        public bool HexagonValuesEqual(Hexagon hex)
        {
            return HexagonValuesEqual(this, hex);
        }

        /// <summary>
        /// Setup this hexagon with the correct coordinates.
        /// </summary>
        /// <param name="x">The x cubal coordinate to use for this hexagon.</param>
        /// <param name="y">The y cubal coordinate to use for this hexagon.</param>
        /// <param name="z">The z cubal coordinate to use for this hexagon.</param>
        public void Setup(int x, int y, int z)
        {
            if (x + y + z != 0)
                throw new ArgumentException("Sum must be zero.");

            _x = x;
            _z = z;
        }

        #endregion Public Methods
    }
}