﻿namespace ReusableScripts.Grids.Hexagons
{
    using MonoBehaviours;
    using Orientation;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public abstract class HexagonGrid : AdvancedMonoBehaviour
    {
        #region Protected Fields

        /// <summary>
        /// The number of columns that the hexagon grid should contain.
        /// </summary>
        [SerializeField]
        protected int _columns;

        /// <summary>
        /// List of the hexagons that belong to this grid.
        /// </summary>
        protected List<Hexagon> _hexagons;

        /// <summary>
        /// The hexagon prefab to use when building the grid.
        /// </summary>
        [SerializeField]
        protected GameObject _hexPrefab;

        /// <summary>
        /// The orientation of the hexes in the grid.
        /// </summary>
        protected IHexOrientation _orientation;

        /// <summary>
        /// The padding to add around all the pieces in the grid.
        /// </summary>
        [SerializeField]
        protected float _padding;

        /// <summary>
        /// Amount to rotate the piece in order for it to be positioned correctly.
        /// </summary>
        [SerializeField]
        protected Quaternion _pieceRotation;

        /// <summary>
        /// The size of the hexagon pieces.
        /// </summary>
        [SerializeField]
        protected Vector3 _pieceSize;

        /// <summary>
        /// Whether these grid pieces should be a pointy top.
        /// </summary>
        [SerializeField]
        protected bool _pointyTop = false;

        /// <summary>
        /// The number of rows that the hexagon grid should contain.
        /// </summary>
        [SerializeField]
        protected int _rows;

        #endregion Protected Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the number of columns that the hexagon grid should contain.
        /// </summary>
        public int Columns
        {
            get
            {
                return _columns;
            }

            set
            {
                _columns = value;
                UpdateGrid();
            }
        }

        /// <summary>
        /// Gets or sets the prefab that should be used as the grids pieces.
        /// </summary>
        public GameObject HexagonPrefab
        {
            get
            {
                return _hexPrefab;
            }
            set
            {
                _hexPrefab = value;
                UpdateGrid();
            }
        }

        /// <summary>
        /// Gets or sets the padding to add around all the pieces in the grid.
        /// </summary>
        public float Padding
        {
            get
            {
                return _padding;
            }
            set
            {
                _padding = value;
                UpdateGrid();
            }
        }

        /// <summary>
        /// Gets or sets the amount that the piece should be rotated by default.
        /// </summary>
        public Quaternion PieceRotation
        {
            get
            {
                return _pieceRotation;
            }
            set
            {
                _pieceRotation = value;
                UpdateGrid();
            }
        }

        /// <summary>
        /// Gets or sets the size of the hexagon pieces.
        /// </summary>
        public Vector3 PieceSize
        {
            get
            {
                return _pieceSize;
            }
            set
            {
                _pieceSize = value;
                SizeUpdate();
                UpdateGrid();
            }
        }

        /// <summary>
        /// Gets or sets whether these grid pieces should be a pointy top.
        /// </summary>
        public bool PointyTop
        {
            get
            {
                return _pointyTop;
            }
            set
            {
                if (_pointyTop == value) return;
                _pointyTop = value;
                if (value)
                {
                    _orientation = new HexPointyTopOrientation();
                }
                else
                {
                    _orientation = new HexFlatTopOrientation();
                }
                UpdateGrid();
            }
        }

        /// <summary>
        /// Gets or sets the number of rows that the hexagon grid should contain.
        /// </summary>
        public int Rows
        {
            get
            {
                return _rows;
            }

            set
            {
                _rows = value;
                UpdateGrid();
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Create the grid using specific algorithms.
        /// </summary>
        public abstract void CreateGrid();

        #endregion Public Methods

        #region Protected Methods

        /// <summary>
        /// Method that will be called during Awake.
        /// </summary>
        /// <remarks>
        /// Use this method instead of Awake so that base calls from AdvancedMonoBehaviour can be
        /// called as well. This will still be called during the Awake call.
        /// </remarks>
        protected override void AdvancedAwake()
        {
            _hexagons = new List<Hexagon>();

            if(_pointyTop)
            {
                _orientation = new HexPointyTopOrientation();
            }
            else
            {
                _orientation = new HexFlatTopOrientation();
            }
        }

        /// <summary>
        /// Clear any of the undefined hexagons in the grid.
        /// </summary>
        protected abstract void ClearUndefinedHexagons();

        /// <summary>
        /// Gets the position of a hexagon in reference to the grid being it's parent.
        /// </summary>
        /// <param name="hexPiece">The hexagon piece to get the location of.</param>
        /// <returns>A vector 3 location for the hexagon piece.</returns>
        protected Vector3 GetLocalPosition(Hexagon hexPiece)
        {
            float xPosition = _orientation.GetXPosition(
                    hexPiece, _hexPrefab.GetComponent<MeshRenderer>().bounds.extents.x, _padding, _pieceSize.x);
                
            float zPosition = _orientation.GetZPosition(
                hexPiece, _hexPrefab.GetComponent<MeshRenderer>().bounds.extents.z, _padding, _pieceSize.z);

            return new Vector3(xPosition, 0, zPosition);
        }

        /// <summary>
        /// Make a hexagon to add to the grid.
        /// </summary>
        /// <param name="x">The x cubal coordinate.</param>
        /// <param name="y">The y cubal coordinate.</param>
        /// <param name="z">The z cubal coordinate.</param>
        protected void MakeHexagon(int x, int y, int z)
        {
            // Check to make sure that the hexagon is a legitimate position
            if (x + y + z != 0)
            {
                throw new ArgumentException(
                    string.Format(
                        "The sum of the three cubal coordinates must be zero.\n{0}(x) + {1}(y) + {2}(z) = {3}",
                        x,
                        y,
                        z,
                        x + y + z));
            }

            // Storage container for the hexagon object
            GameObject hexagonObj;

            // Set the rotation that the piece will have when it's generated
            Quaternion pieceRotation = _pieceRotation * _orientation.StartingRotation;

            // Instantiate the object
            hexagonObj = (GameObject)Instantiate(_hexPrefab, Vector3.zero, pieceRotation);

            // Make the hexagon component
            Hexagon hexComponent = hexagonObj.AddComponent<Hexagon>();
            hexComponent.Setup(x, y, z);
            _hexagons.Add(hexComponent);

            // Set the parent of the piece and it's location
            hexagonObj.transform.SetParent(transform);
            hexagonObj.transform.position = GetLocalPosition(hexComponent);

            SizeUpdate(hexagonObj);
        }

        /// <summary>
        /// Update the hexagon to show correctly with new parameters.
        /// </summary>
        protected void UpdateHexagon(Hexagon hexagon)
        {
            hexagon.transform.localPosition = GetLocalPosition(hexagon);
            hexagon.transform.localRotation = _pieceRotation * _orientation.StartingRotation;
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Updates the size of a hexagon.
        /// </summary>
        /// <param name="hex"></param>
        private void SizeUpdate(GameObject hex)
        {
            hex.transform.localScale = Vector3.Scale(_hexPrefab.transform.localScale, _pieceSize);
        }

        /// <summary>
        /// Update the size of the hexagons.
        /// </summary>
        private void SizeUpdate()
        {
            foreach (GameObject curGameObject in _hexagons.Select(x => x.gameObject))
            {
                SizeUpdate(curGameObject);
            }
        }

        /// <summary>
        /// Updates the grid with the current set values.
        /// </summary>
        private void UpdateGrid()
        {
            // Make sure all information is present in order to update
            if (_rows == 0 || _columns == 0 || _hexPrefab == null || _hexagons == null)
            {
                return;
            }

            foreach (Hexagon curHexagon in _hexagons)
            {
                UpdateHexagon(curHexagon);
            }
        }

        #endregion Private Methods
    }
}