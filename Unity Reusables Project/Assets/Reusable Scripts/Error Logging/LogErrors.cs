﻿//-----------------------------------------------------------------------
// <copyright file="LogErrors.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ErrorLogging
{
    using MonoBehaviours;
    using System;
    using System.IO;
    using UnityEngine;

    /// <summary>
    /// Logs errors in a file in the persistent data path location given by Unity. 
    /// </summary>
    /// <remarks>
    /// It is highly recommended that you do not attach this to a game object that you do not want
    /// persisting through scenes. The persistent singleton monobehaviour that it inherits from will
    /// keep the gameobject alive until it is explicitly destroyed by ending the application or by
    /// the user. Recommend attaching to an empty game object.
    /// 
    /// The script will be disabled if it's in the editor or not in a debug build. So during live
    /// deployment it will not keep a running log.
    /// </remarks>
    public class LogErrors : PersistentSingletonMonoBehaviour<LogErrors>
    {
        #region Private Fields

        /// <summary>
        /// The header for any assert message. 
        /// </summary>
        private const string _assertHeader =
            "===========================================================================\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$-Assert-$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n===========================================================================";

        /// <summary>
        /// The header for any error message. 
        /// </summary>
        private const string _errorHeader =
            "===========================================================================\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n===========================================================================";

        /// <summary>
        /// The header for any exception message. 
        /// </summary>
        private const string _exceptionHeader =
            "===========================================================================\nxxxxxxxxxxxxxxxxx!!!!!!!!!!!!!!!!Exception!!!!!!!!!!!!!!!!!xxxxxxxxxxxxxxxx\n===========================================================================";

        /// <summary>
        /// The header for any log message. 
        /// </summary>
        private const string _logHeader =
            "===========================================================================\n------------------------------------Log------------------------------------\n===========================================================================";

        /// <summary>
        /// The header for any warning message. 
        /// </summary>
        private const string _warningHeader =
            "===========================================================================\n---------------------------------!Warning!---------------------------------\n===========================================================================";

        /// <summary>
        /// The file name of the log that is being saved to for this build. 
        /// </summary>
        private string _logFileName = null;

        /// <summary>
        /// The folder path to store log files. 
        /// </summary>
        private string _logFolderPath = null;

        #endregion Private Fields

        #region Private Properties

        /// <summary>
        /// Gets the path to the log file. 
        /// </summary>
        private string _FilePath
        {
            get
            {
                return string.Format("{0}/{1}", _logFolderPath, _logFileName);
            }
        }

        #endregion Private Properties

        #region Protected Methods

        /// <summary>
        /// Method that will be called during Awake. 
        /// </summary>
        /// <remarks>
        /// Use this method instead of Awake so that base calls from AdvancedMonoBehaviour can be
        /// called as well. This will still be called during the Awake call.
        /// </remarks>
        protected override void AdvancedAwake()
        {
            // Needed to establish the singleton and its persistence 
            base.AdvancedAwake();

            // If we're in the editor or not in debug mode, then no log file is needed to be generated.
#if UNITY_EDITOR
            enabled = false;
            Destroy(gameObject);
#else
            if (!Debug.isDebugBuild)
            {
                enabled = false;
                Destroy(gameObject);
            }
#endif
        }

        /// <summary>
        /// Method that will be called during OnDestroy. 
        /// </summary>
        /// <remarks>
        /// Use this method instead of OnDestroy so that base calls from SingletonMonoBehaviour can
        /// be called as well. This will still be called during the OnDestroy call.
        /// </remarks>
        protected override void AdvancedOnDestroy()
        {
            Application.logMessageReceivedThreaded -= MessageReceived;
        }

#endregion Protected Methods

#region Private Methods

        /// <summary>
        /// Called whenever a message is received to the call stack. 
        /// </summary>
        /// <param name="message">
        /// The string that's logged in the message. 
        /// </param>
        /// <param name="stackTrace">
        /// The stack trace text that was sent with the message. 
        /// </param>
        /// <param name="type">
        /// The type of log that this is. 
        /// </param>
        private void MessageReceived(string message, string stackTrace, LogType type)
        {
            string newMessage;

            switch (type)
            {
                case LogType.Assert:
                    newMessage = string.Format("{0}\n{1} - {2}\n\n{3}",
                        _assertHeader, DateTime.Now, message, stackTrace);
                    break;

                case LogType.Error:
                    newMessage = string.Format("{0}\n{1} - {2}\n\n{3}",
                        _errorHeader, DateTime.Now, message, stackTrace);
                    break;

                case LogType.Exception:
                    newMessage = string.Format("{0}\n{1} - {2}\n\n{3}",
                        _exceptionHeader, DateTime.Now, message, stackTrace);
                    break;

                case LogType.Log:
                    newMessage = string.Format("{0}\n{1} - {2}\n\n{3}",
                        _logHeader, DateTime.Now, message, stackTrace);
                    break;

                case LogType.Warning:
                    newMessage = string.Format("{0}\n{1} - {2}\n\n{3}",
                        _warningHeader, DateTime.Now, message, stackTrace);
                    break;

                default:
                    newMessage = "Problem logging error message.";
                    break;
            }

            using (StreamWriter stream = File.AppendText(_FilePath))
            {
                stream.WriteLine(newMessage);
            }
        }

        /// <summary>
        /// Called when the behavior becomes disabled or inactive. 
        /// </summary>
        private void OnDisable()
        {
            using (StreamWriter stream = File.AppendText(_FilePath))
            {
                stream.WriteLine(
                    "--------------------------------End Session--------------------------------\nSession Ended: {0}",
                    DateTime.Now);
            }
            Application.logMessageReceivedThreaded -= MessageReceived;
        }

        /// <summary>
        /// Called when the object becomes enabled and active. 
        /// </summary>
        private void OnEnable()
        {
            Application.logMessageReceivedThreaded += MessageReceived;
        }

        /// <summary>
        /// Called on the frame when a script is enabled just before any of the Update methods is
        /// called the first time.
        /// </summary>
        private void Start()
        {
            // Get the folder path 
            _logFolderPath = string.Format("{0}/{1}/{2}", Application.persistentDataPath, "Logs", DateTime.Now.ToString("dd-MM-yyy"));
            if (!Directory.Exists(_logFolderPath))
            {
                Directory.CreateDirectory(_logFolderPath);
            }

            // Get the file name of the log file 
            _logFileName = string.Format("Session Start - {0}.txt", DateTime.Now.ToString("HH-mm-ss"));
            if (!File.Exists(_FilePath))
            {
                File.Create(_FilePath).Dispose();
            }
        }

#endregion Private Methods
    }
}