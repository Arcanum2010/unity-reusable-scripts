﻿//-----------------------------------------------------------------------
// <copyright file="Singleton.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.Generic
{
    /// <summary>
    /// Generic C# class of a singleton reference. 
    /// </summary>
    /// <typeparam name="T">
    /// The class type that this singleton is representing. 
    /// </typeparam>
    public class Singleton<T> where T : class, new()
    {
        #region Private Fields

        /// <summary>
        /// The private reference holder to the singleton instance of T class. 
        /// </summary>
        private static T _instance;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the Singleton class. 
        /// </summary>
        public Singleton()
        {
            _instance = this as T;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the reference to the instance of this class. 
        /// </summary>
        public static T Instance
        {
            get
            {
                // If instance is not currently set, then create a new instance and set it. 
                if (_instance == null) return _instance = new T();
                return _instance;
            }
        }

        #endregion Public Properties
    }
}