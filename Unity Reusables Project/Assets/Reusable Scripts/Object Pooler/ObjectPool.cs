﻿//-----------------------------------------------------------------------
// <copyright file="ObjectPool.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ObjectPooler
{
    using MonoBehaviours;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// Base instructions for running a pool of game objects. 
    /// </summary>
    /// <typeparam name="T">
    /// The class that is the pooler. 
    /// </typeparam>
    public abstract class ObjectPool<T> : AutoGeneratingSingletonMonoBehaviour<T> where T : MonoBehaviour
    {
        #region Private Fields

        /// <summary>
        /// The prefab that will be made for the pooler to monitor. 
        /// </summary>
        private GameObject _pooledObjectPrefab;

        /// <summary>
        /// List of the pooled objects. 
        /// </summary>
        private List<GameObject> _pooledObjects;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the parent of the object when it is active in the scene. 
        /// </summary>
        public abstract Transform ActiveParent { get; set; }

        #endregion Public Properties

        #region Protected Properties

        /// <summary>
        /// Gets a value indicating whether this pool is allowed to grow or not. 
        /// </summary>
        protected abstract bool _grow { get; }

        /// <summary>
        /// Gets the maximum number of pooled objects that should be allowed to exist in the pool at
        /// any given moment in time.
        /// </summary>
        /// <remarks>
        /// Set to 0 if the maximum is infinite. 
        /// </remarks>
        protected abstract int _max { get; }

        /// <summary>
        /// Gets the minimum number of pooled objects that should exist in the pool at any given
        /// moment in time.
        /// </summary>
        /// <remarks>
        /// If the minimum is 0, then no objects will be made at the beginning of the scene. 
        /// </remarks>
        protected abstract int _min { get; }

        /// <summary>
        /// Gets or sets the prefab that will be made for the pooler to monitor. 
        /// </summary>
        protected GameObject _PooledObjectPrefab
        {
            get
            {
                return _pooledObjectPrefab;
            }

            set
            {
                _pooledObjectPrefab = value;
                BufferPrefabPool();
            }
        }

        /// <summary>
        /// Gets the list of pooled objects. 
        /// </summary>
        protected List<GameObject> _PooledObjects
        {
            get
            {
                return _pooledObjects;
            }
        }

        #endregion Protected Properties

        #region Public Methods

        /// <summary>
        /// Gets all the objects in the pool that are currently active. 
        /// </summary>
        /// <returns>
        /// A list of game objects that are currently active in the scene. 
        /// </returns>
        public virtual GameObject[] GetAllActivePooledObjects()
        {
#if UNITY_IOS
            List<GameObject> activeGameObjects = new List<GameObject>();
            foreach(GameObject curGameObject in _pooledObjects)
            {
                if(curGameObject.activeInHierarchy)
                {
                    activeGameObjects.Add(curGameObject);
                }
            }
            return activeGameObjects.ToArray();
#else
            return _pooledObjects.Where(x => x.activeInHierarchy).ToArray();
#endif
        }

        /// <summary>
        /// Gets all the objects in the pool that are currently inactive. 
        /// </summary>
        /// <returns>
        /// A list of game objects that are not currently active in the scene. 
        /// </returns>
        public virtual GameObject[] GetAllInactivePooledObjects()
        {
#if UNITY_IOS
            List<GameObject> inactiveGameObjects = new List<GameObject>();
            foreach(GameObject curGameObject in _pooledObjects)
            {
                if(!curGameObject.activeInHierarchy)
                {
                    inactiveGameObjects.Add(curGameObject);
                }
            }
            return inactiveGameObjects.ToArray();
#else
            return _pooledObjects.Where(x => !x.activeInHierarchy).ToArray();
#endif
        }

        /// <summary>
        /// Call to get a game object from the pooler. 
        /// </summary>
        /// <returns>
        /// An inactive object from the pool. 
        /// </returns>
        public virtual GameObject GetPooledObject()
        {
            // Find the first object in the pool that isn't active in the hierarchy 
            GameObject returnObject = _pooledObjects.FirstOrDefault(x => !x.activeInHierarchy);

            // If the return object isn't null, return it 
            if (returnObject != null)
            {
                PlacePooledObjectAsChildOfActiveParent(returnObject);
                return returnObject;
            }

            // Compensate for there being no max 
            bool maxReached = _max == 0 ? false : _pooledObjects.Count >= _max;

            // If allowed, then make a new pooled object and return it 
            if (_grow && !maxReached)
            {
                returnObject = CreatePooledObject();
                PlacePooledObjectAsChildOfActiveParent(returnObject);
                return returnObject;
            }

            // If not able to make new ones and all are being used, then null is returned. 
            return null;
        }

        /// <summary>
        /// Call to get numerous game objects from the pooler. 
        /// </summary>
        /// <param name="amount">
        /// The number of game objects you want to retrieve from the pooler. 
        /// </param>
        /// <returns>
        /// An array of game objects that were retrieved from the pooler. 
        /// </returns>
        public virtual GameObject[] GetPooledObject(int amount)
        {
            // Get the first number of non-active game objects in the pool up to the desired amount
#if UNITY_IOS
            List<GameObject> returnedGameObjects = new List<GameObject>();
            foreach (GameObject curGameObject in _pooledObjects)
            {
                if(!curGameObject.activeInHierarchy)
                {
                    returnedGameObjects.Add(curGameObject);

                    if(returnedGameObjects.Count == amount)
                    {
                        break;
                    }
                }
            }
#else
            List<GameObject> returnedGameObjects = _pooledObjects.Where(x => !x.activeInHierarchy).Take(amount).ToList();
#endif

            // If this was the number of game objects needed then it returns the list 
            if (returnedGameObjects.Count >= amount)
            {
                PlacePooledObjectAsChildOfActiveParent(returnedGameObjects);

                // Return the list of game objects 
                return returnedGameObjects.ToArray();
            }

            // If more are required then generate them 
            while (!(_max == 0 ? false : _pooledObjects.Count > _max) && _grow && returnedGameObjects.Count < amount)
            {
                GameObject newObject = CreatePooledObject();
                newObject.transform.SetParent(ActiveParent);
                returnedGameObjects.Add(newObject);
            }

            // Put all of the game objects as the child of the active parent 
            PlacePooledObjectAsChildOfActiveParent(returnedGameObjects);

            // Return the list of game objects 
            return returnedGameObjects.ToArray();
        }

#endregion Public Methods

#region Protected Methods

        /// <summary>
        /// Method that will be called during Awake. 
        /// </summary>
        protected override void AdvancedAwake()
        {
            // Must be called to keep the singleton functional 
            base.AdvancedAwake();

            _pooledObjects = _grow || _max == 0 ? new List<GameObject>() : new List<GameObject>(_max);
        }

        /// <summary>
        /// Creates a new instance of the prefab game object. 
        /// </summary>
        /// <returns>
        /// The new instance of the prefab game object. 
        /// </returns>
        protected virtual GameObject CreatePooledObject()
        {
            // Create the new instance of the object 
            GameObject newObject = Instantiate(_pooledObjectPrefab);

            // Add the instance to the pooled objects list 
            _pooledObjects.Add(newObject);

            // Set the pooler variable in the pooled object to this pooler 
            newObject.GetComponent<PooledObject<T>>().Pooler = this;

            // Return the instance 
            return newObject;
        }

#endregion Protected Methods

#region Private Methods

        /// <summary>
        /// Buffers the prefab pool with the amount of objects equal to the minimum specified for
        /// this class.
        /// </summary>
        private void BufferPrefabPool()
        {
            for (int i = 0; i < _min; i++)
            {
                // Create the new instance of the object 
                GameObject newBuffer = CreatePooledObject();

                // Set the object to not active 
                newBuffer.SetActive(false);

                // Set the parent of the game object to the object poolers object 
                newBuffer.transform.SetParent(transform);
            }
        }

        /// <summary>
        /// Place the object as a child of the active parent that is defined. 
        /// </summary>
        /// <param name="pooledObject">
        /// The object that should be placed as a child of the active parent. 
        /// </param>
        private void PlacePooledObjectAsChildOfActiveParent(GameObject pooledObject)
        {
            pooledObject.transform.SetParent(ActiveParent);
        }

        /// <summary>
        /// Place the objects as children of the active parent that is defined. 
        /// </summary>
        /// <param name="pooledObjects">
        /// The objects that should be placed as children of the active parent. 
        /// </param>
        private void PlacePooledObjectAsChildOfActiveParent(GameObject[] pooledObjects)
        {
            foreach (GameObject curGameObject in pooledObjects)
            {
                PlacePooledObjectAsChildOfActiveParent(curGameObject);
            }
        }

        /// <summary>
        /// Place the objects as children of the active parent that is defined. 
        /// </summary>
        /// <param name="pooledObjects">
        /// The objects that should be placed as children of the active parent. 
        /// </param>
        private void PlacePooledObjectAsChildOfActiveParent(List<GameObject> pooledObjects)
        {
            foreach (GameObject curGameObject in pooledObjects)
            {
                PlacePooledObjectAsChildOfActiveParent(curGameObject);
            }
        }

#endregion Private Methods
    }
}