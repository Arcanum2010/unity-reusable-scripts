﻿//-----------------------------------------------------------------------
// <copyright file="ColorExtensionMethods.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ExtensionMethods
{
    using System.Globalization;
    using UnityEngine;

    /// <summary>
    /// Extension methods for the Color class from Unity.
    /// </summary>
    public static class ColorExtensionMethods
    {
        #region Public Methods

        /// <summary>
        /// Converts Unity color over to a hexadecimal value representation. 
        /// </summary>
        /// <param name="color">
        /// The color to be converted into hex form. 
        /// </param>
        /// <returns>
        /// A string representation of the color value passed in. 
        /// </returns>
        public static string ColorToHex(this Color color)
        {
            Color32 convertedColor = (Color32)color;
            return string.Format("{0}{1}{2}", convertedColor.r.ToString("X2"), convertedColor.g.ToString("X2"), convertedColor.b.ToString("X2"));
        }

        /// <summary>
        /// Converts a string hexidecimal value to a Unity color instance. 
        /// </summary>
        /// <param name="hex">
        /// The hex value as a string to be converted to a color. 
        /// </param>
        /// <returns>
        /// The Unity Color that was made. 
        /// </returns>
        public static Color HexToColor(this string hex)
        {
            byte r = byte.Parse(hex.Substring(0, 2), NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        }

        #endregion Public Methods
    }
}