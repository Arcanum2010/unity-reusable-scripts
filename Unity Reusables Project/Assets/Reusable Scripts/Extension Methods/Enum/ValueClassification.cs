﻿//-----------------------------------------------------------------------
// <copyright file="ValueClassification.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ExtensionMethods
{
    using System;

    /// <summary>
    /// Class to simplify narrowing values between a ulong and long since either value should cover
    /// any lesser value.
    /// </summary>
    /// <remarks>
    /// This class code was retrieved from a coder's website by the name of Hugo Bonacci. I can't
    /// take any credit for it, it was all him. If Hugo stumbles across this, I hope that you are
    /// not upset by the addition of this code in this library, but if so, please contact me and it
    /// will be removed immediately. Thank you!
    /// 
    /// Original Code Post: http://hugoware.net/blog/enumeration-extensions-2-0
    /// 
    /// Hugo's Web-site: http://hugoware.net/
    /// </remarks>
    public class ValueClassification
    {
        #region Public Fields

        /// <summary>
        /// The signed value of the value passed in. 
        /// </summary>
        public long? Signed;

        /// <summary>
        /// The unsigned value of the value passed in. 
        /// </summary>
        public ulong? Unsigned;

        #endregion Public Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueClassification" /> class. 
        /// </summary>
        /// <param name="value">
        /// The value. 
        /// </param>
        /// <param name="type">
        /// The type. 
        /// </param>
        /// <exception cref="ArgumentException">
        /// Value provided is not an enumerated type! 
        /// </exception>
        public ValueClassification(object value, Type type)
        {
            // Make sure that we have an enum to work with 
            if (!type.IsEnum)
            {
                throw new ArgumentException("Value provided is not an enumerated type!");
            }

            // Then check for the enumerated value 
            Type compare = Enum.GetUnderlyingType(type);

            // If this is an unsigned long then the only value that can hold it would be a ulong.
            // Otherwise, a long should cover anything else.
            if (compare.Equals(typeof(long)) || compare.Equals(typeof(ulong)))
            {
                Unsigned = Convert.ToUInt64(value);
            }
            else
            {
                Signed = Convert.ToInt64(value);
            }
        }

        #endregion Public Constructors
    }
}