﻿//-----------------------------------------------------------------------
// <copyright file="EnumExtensionMethods.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ExtensionMethods
{
    using System;

    /// <summary>
    /// Extension methods for enum values. 
    /// </summary>
    /// <remarks>
    /// Parts of this class' code was retrieved from a coder's website by the name of Hugo Bonacci.
    /// I can't take any credit for it, it was all him. I will mark the methods that were written by
    /// him in the remarks of the XML comments. If Hugo stumbles across this, I hope that you are
    /// not upset by the addition of this code in this library, but if so, please contact me and it
    /// will be removed immediately. Thank you!
    /// 
    /// Original Code Post: http://hugoware.net/blog/enumeration-extensions-2-0
    /// 
    /// Hugo's Web-site: http://hugoware.net/
    /// </remarks>
    public static class EnumExtensionMethods
    {
        #region Public Methods

        /// <summary>
        /// Determines whether [has] [the specified value]. 
        /// </summary>
        /// <typeparam name="T">
        /// The type of enum value to be used. 
        /// </typeparam>
        /// <param name="value">
        /// The value. 
        /// </param>
        /// <param name="check">
        /// The check. 
        /// </param>
        /// <returns>
        /// True if the value has the requested type. 
        /// </returns>
        /// <remarks>
        /// This is one of Hugo Bonacci's methods. 
        /// </remarks>
        public static bool Has<T>(this Enum value, T check)
        {
            // Reference Variables 
            Type type;
            object result;

            // Determine the values 
            ValueClassification parsed = DetermineValues<T>(value, check, out result, out type);
            if (parsed.Signed is long)
            {
                return (Convert.ToInt64(value) & (long)parsed.Signed) == parsed.Signed;
            }
            else if (parsed.Unsigned is ulong)
            {
                return (Convert.ToUInt64(value) & (ulong)parsed.Unsigned) == parsed.Unsigned;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds the specified value. 
        /// </summary>
        /// <typeparam name="T">
        /// The type of enum to be added to the current. 
        /// </typeparam>
        /// <param name="value">
        /// The type. 
        /// </param>
        /// <param name="append">
        /// The value. 
        /// </param>
        /// <returns>
        /// The new enum value containing the previous and new value combined. 
        /// </returns>
        /// <remarks>
        /// This is one of Hugo Bonacci's methods. 
        /// </remarks>
        public static T Include<T>(this Enum value, T append)
        {
            // Reference variables 
            object result;
            Type type;

            // Determine the values 
            ValueClassification parsed = DetermineValues<T>(value, append, out result, out type);
            if (parsed.Signed is long)
            {
                result = Convert.ToInt64(value) | (long)parsed.Signed;
            }
            else if (parsed.Unsigned is ulong)
            {
                result = Convert.ToUInt64(value) | (ulong)parsed.Unsigned;
            }

            // Return the final value 
            return (T)Enum.Parse(type, result.ToString());
        }

        /// <summary>
        /// Missing the specified value. 
        /// </summary>
        /// <typeparam name="T">
        /// The type of enum value to be used. 
        /// </typeparam>
        /// <param name="obj">
        /// The given enum that will be used for checks. 
        /// </param>
        /// <param name="value">
        /// The value to see if the enum does not contain. 
        /// </param>
        /// <returns>
        /// True if the enum does not contain the specified value. 
        /// </returns>
        /// <remarks>
        /// This is one of Hugo Bonacci's methods. 
        /// </remarks>
        public static bool Missing<T>(this Enum obj, T value)
        {
            return !Has(obj, value);
        }

        /// <summary>
        /// Removes the specified remove. 
        /// </summary>
        /// <typeparam name="T">
        /// The type of enum to be removed to the current. 
        /// </typeparam>
        /// <param name="value">
        /// The value. 
        /// </param>
        /// <param name="remove">
        /// The remove. 
        /// </param>
        /// <returns>
        /// The new enum value containing the new enum value without the identified value. 
        /// </returns>
        /// <remarks>
        /// This is one of Hugo Bonacci's methods. 
        /// </remarks>
        public static T Remove<T>(this Enum value, T remove)
        {
            // Reference variables 
            Type type;
            object result;

            // Determine the values 
            ValueClassification parsed = DetermineValues<T>(value, remove, out result, out type);
            if (parsed.Signed is long)
            {
                result = Convert.ToInt64(value) & ~(long)parsed.Signed;
            }
            else if (parsed.Unsigned is ulong)
            {
                result = Convert.ToUInt64(value) & ~(ulong)parsed.Unsigned;
            }

            // Return the final value 
            return (T)Enum.Parse(type, result.ToString());
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Determines the values for calculations. 
        /// </summary>
        /// <typeparam name="T">
        /// The type of enumerator. 
        /// </typeparam>
        /// <param name="value">
        /// The original enum value. 
        /// </param>
        /// <param name="item">
        /// The item that will be manipulated to the original enum. 
        /// </param>
        /// <param name="result">
        /// The final resulting enum value. 
        /// </param>
        /// <param name="type">
        /// The type of value this enum is housing. 
        /// </param>
        /// <returns>
        /// the value classification of the enumerator. 
        /// </returns>
        /// <remarks>
        /// This is an extrapolation of Hugo Bonacci's code. 
        /// </remarks>
        private static ValueClassification DetermineValues<T>(Enum value, T item, out object result, out Type type)
        {
            type = value.GetType();
            result = value;
            return new ValueClassification(item, type);
        }

        #endregion Private Methods
    }
}