﻿//-----------------------------------------------------------------------
// <copyright file="GenericExtensionMethods.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.ExtensionMethods
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Extension methods for generic class types.
    /// </summary>
    public static class GenericExtensionMethods
    {
        #region Public Methods

        /// <summary>
        /// Randomizes the order of an array.
        /// </summary>
        /// <typeparam name="T">The type of array this is.</typeparam>
        /// <param name="array">The array that is being modified.</param>
        /// <returns>The randomized array.</returns>
        public static void RandomizeOrder<T>(this T[] array)
        {
            // Create the list and randomizer that will be needed
            List<KeyValuePair<int, T>> reorganizer = new List<KeyValuePair<int, T>>();
            Random randomizer = new Random();

            // Move the array into a new list with random numbers
            foreach (T curT in array)
            {
                reorganizer.Add(new KeyValuePair<int, T>(randomizer.Next(), curT));
            }

            // Sort by the random values, and then return the new array randomized
            reorganizer.Sort((a, b) => a.Key.CompareTo(b.Key));

            //Reassign the values
            int index = 0;
            foreach (KeyValuePair<int, T> pair in reorganizer)
            {
                array[index] = pair.Value;
                index++;
            }
        }

        #endregion Public Methods
    }
}
