﻿//-----------------------------------------------------------------------
// <copyright file="GetInputKeyCode.cs">
//  Copyright (c) 2015, Sam Bolton
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//  1. Redistributions of source code must retain the above copyright notice,
//  this list of conditions and the following disclaimer.
//  2. Redistributions in binary form must reproduce the above copyright notice,
//  this list of conditions and the following disclaimer in the documentation
//  and/or other materials provided with the distribution.
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
//  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

namespace ReusableScripts.HelperScripts
{
    using MonoBehaviours;
    using System;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// A script that will find the key code of a button that is currently being pressed. 
    /// </summary>
    public class GetInputKeyCode : AdvancedMonoBehaviour
    {
        #region Protected Methods

        /// <summary>
        /// Method that will be called during Awake. 
        /// </summary>
        /// <remarks>
        /// Use this method instead of Awake so that base calls from AdvancedMonoBehaviour can be
        /// called as well. This will still be called during the Awake call.
        /// </remarks>
        protected override void AdvancedAwake()
        {
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Called every frame, if the MonoBehaviour is enabled. 
        /// </summary>
        private void Update()
        {
            foreach (KeyCode curCode in ((KeyCode[])Enum.GetValues(typeof(KeyCode))).Where(Input.GetKeyDown))
            {
                Debug.Log(curCode);
            }
        }

        #endregion Private Methods
    }
}